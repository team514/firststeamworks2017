package frc.robot;

/**
 * The RobotMap is a mapping from the ports sensors and actuators are wired into
 * to a variable name. This provides flexibility changing wiring, makes checking
 * the wiring easier and significantly reduces the number of magic numbers
 * floating around.
 */
public class RobotMap {
	// For example to map the left and right motors, you could define the
	// following variables to use with your drivetrain subsystem.
	// public static int leftMotor = 1;
	// public static int rightMotor = 2;

	// If you are using multiple modules, make sure to define both the port
	// number and the module. For example you with a rangefinder:
	// public static int rangefinderPort = 1;
	// public static int rangefinderModule = 1;
	
	//All Autonomous variables and constants go here...
		public static final double wait = 0.25;
		public static final double autoX = 0.55;
		public static final double autoY = -0.55;
		public static final double autoZ = 0.55;
		public static final double autoYM = -0.4;
		public static final int autoDefaultDistance = 1188;		//12 ft (131 encoder ticks = 1ft)
		public static final int autoDefaultDistance2 = 197;
		public static final int autoGearDistance = 841;		//12 ft (131 encoder ticks = 1ft)
		public static final int autoFuelDistance = 385;
		public static final int autoFuelDistance2 = 655;
		public static final int autoFuelDistance3 = 786;
		public static final int autoPinDistance = 917;  		//12 ft (131 encoder ticks = 1ft)
		public static final int autoPinMDistance = 1703;
		public static final int autoPinMDistance2 = 1441;
		public static final int autoFinishDistance = 197;
		public static final double autoFuelTurn = 37;
		public static final double turn = 60.0;
		//public static final int autoFinish = 262;
		public static final int led = 0;
		public static final int autoFuelMDistance = 131;
		public static final double autoFuelMRotate = 5.0;
		
		
		//Coerce to Range Variables
		public static final double C2R_inputMin = -4.0;
        public static final double C2R_inputMax = 4.0;
        public static final double C2R_outputMin = -1.0;
        public static final double C2R_outputMax = 1.0;
        
        //Coerce to Range Magic Variables
		public static final int C2RM_inputMin = 0;
        public static final int C2RM_inputMax = 640;
        public static final double C2RM_outputMin = -0.5;
        public static final double C2RM_outputMax = 0.5;
	
	
	//All DriveUtil variables and constants go here...
		public static final int leftrear = 1;
		public static final int leftfront = 0;	
		public static final int rightrear = 3;
		public static final int rightfront = 2;
		public static final int leftfrontforward = 2;
		public static final int leftfrontbackward = 3;
		public static final int leftrearforward = 0;
		public static final int leftrearbackward = 1;
		public static final int rightfrontforward = 5;
		public static final int rightfrontbackward = 4;
		public static final int rightrearforward = 6;
		public static final int rightrearbackward = 7;
	
	//All LiftUtil variables and constants go here...
		public static final int liftmotor = 1;   //CANTalon, CAN ID
		public static final double liftspeed = 1.0;
		
	//All GearUtil variables and constants go here...
		public static final int gearGrip = 7;
		public static final int pegSwitch = 9;
	
	//All FuelUtil variables and constants go here...
		public static final int floorControl = 2;
		public static final int basketLift = 3;
		public static final int basketTopSwitch = 8;

	
	//All Vision Processing Pipeline variables and constants go here...
		public static final double pintapewidth = 2.0;
		public static final double cameralensangle = 33.5;
		public static final double yresolution = 480.0;
		public static final double xresolution = 640.0;
		public static final double RECTANGULARITY_LIMIT = 90.0;
		public static final double ASPECT_RATIO_LIMIT = 90.0;
		public static final double idealAspectRatio = 0.4;
		public static final int xresUsb = 640;
		public static final int yresUsb = 320;
	
	//Right Joystick Button Map goes here...
		public static final int ResetGyro = 12;
		public static final int ResetEncoders = 11;
		public static final int DriveMode = 4;
		public static final int SquareInputs = 3;
	
	
	//Left Joystick Button Map goes here...
	
	
	//Controller Button Map goes here...
	//LogiTech F310 Button Mapping X/D Switch = D, Direct Input
		//Axis
		public static final int kLeftXAxisNum = 0;
		public static final int kLeftYAxisNum = 1;
		public static final int kRightXAxisNum = 2;
		public static final int kRightYAxisNum = 3;
		
		//For DPad, use controller.getPOV();
		//public static final int kDPadXAxisNum = 5;
		//public static final int kDPadYAxisNum = 6;
		
		public static final int kXButtonNum = 1;
		public static final int kAButtonNum = 2;
		public static final int kBButtonNum = 3;
		public static final int kYButtonNum = 4;
		public static final int kLeftBumperNum = 5;
		public static final int kRightBumperNum = 6;
		public static final int kLeftTriggerNum = 7;
		public static final int kRightTriggerNum = 8;
		public static final int kBackButtonNum = 9;
		public static final int kStartButtonNum = 10;
		public static final int kLeftStickButtonNum = 11;
		public static final int kRightStickButtonNum = 12;
}
