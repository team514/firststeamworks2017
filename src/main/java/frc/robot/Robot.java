package frc.robot;

import edu.wpi.cscore.UsbCamera;
import edu.wpi.first.wpilibj.AnalogOutput;
import edu.wpi.first.wpilibj.CameraServer;
import edu.wpi.first.wpilibj.IterativeRobot;
import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.command.Scheduler;
import edu.wpi.first.wpilibj.livewindow.LiveWindow;
import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.robot.commands.AutoCenterGear;
import frc.robot.commands.AutoDefault;
import frc.robot.commands.AutoFuel;
import frc.robot.commands.AutoGear;
import frc.robot.commands.AutoMagic;
import frc.robot.subsystems.DriveUtil;
import frc.robot.subsystems.FuelUtil;
import frc.robot.subsystems.GearUtil;
import frc.robot.subsystems.LiftUtil;
import frc.robot.subsystems.VisionUtil;

/**
 * The VM is configured to automatically run this class, and to call the
 * functions corresponding to each mode, as described in the IterativeRobot
 * documentation. If you change the name of this class or the package after
 * creating this project, you must also update the manifest file in the resource
 * directory.
 */
@SuppressWarnings("deprecation")
public class Robot extends IterativeRobot {

	public static final DriveUtil driveUtil = new DriveUtil();
	public static final LiftUtil liftUtil = new LiftUtil();
	public static final GearUtil gearUtil = new GearUtil();
	public static final FuelUtil fuelUtil = new FuelUtil();
	public static final VisionUtil visionUtil = new VisionUtil();
	
	AnalogOutput led;
	
	UsbCamera cam;
		
	public static OI oi;

	Command autonomousCommand;
	SendableChooser<Command> chooser = new SendableChooser<>();

	/**
	 * This function is run when the robot is first started up and should be
	 * used for any initialization code.
	 */
	@Override
	public void robotInit() {
		oi = new OI();

		//Set up Autonomous Commands here...
		chooser.addDefault("Gear Auto Left: ", new AutoGear(true));
		chooser.addObject("Default Auto: ", new AutoDefault());
		chooser.addObject("Gear Auto Right: ", new AutoGear(false));
		chooser.addObject("Left Fuel: ", new AutoFuel(false));
		chooser.addObject("Right Fuel: ", new AutoFuel(true));
		chooser.addObject("Left Auto Magic : ", new AutoMagic(false));
		chooser.addObject("Right Auto Magic : ", new AutoMagic(true));
		chooser.addObject("Center Gear: ", new AutoCenterGear());
		SmartDashboard.putData("Auto mode", chooser);
		
		Robot.visionUtil.startVisionProcessing();
		
		// Calibrate Gyro
		Robot.driveUtil.calibrateGyro();
		
		//Set up the Cameras here...
		cam = CameraServer.getInstance().startAutomaticCapture();
		
		led = new AnalogOutput(RobotMap.led);
		
		//Turn on the LEDs
		led.setVoltage(3.0);
	}

	/**
	 * This function is called once each time the robot enters Disabled mode.
	 * You can use it to reset any subsystem information you want to clear when
	 * the robot is disabled.
	 */
	@Override
	public void disabledInit() {

	}

	@Override
	public void disabledPeriodic() {
		Scheduler.getInstance().run();
		updateStatus();
	}

	/**
	 * This autonomous (along with the chooser code above) shows how to select
	 * between different autonomous modes using the dashboard. The sendable
	 * chooser code works with the Java SmartDashboard. If you prefer the
	 * LabVIEW Dashboard, remove all of the chooser code and uncomment the
	 * getString code to get the auto name from the text box below the Gyro
	 *
	 * You can add additional auto modes by adding additional commands to the
	 * chooser code above (like the commented example) or additional comparisons
	 * to the switch structure below with additional strings & commands.
	 */
	@Override
	public void autonomousInit() {
		autonomousCommand = new AutoGear(true);
		Robot.driveUtil.resetEncoders();
		Robot.driveUtil.resetGyro();

		/*
		 * String autoSelected = SmartDashboard.getString("Auto Selector",
		 * "Default"); switch(autoSelected) { case "My Auto": autonomousCommand
		 * = new MyAutoCommand(); break; case "Default Auto": default:
		 * autonomousCommand = new ExampleCommand(); break; }
		 */

		// schedule the autonomous command (example)
		if (autonomousCommand != null)
			autonomousCommand.start();
	}

	/**
	 * This function is called periodically during autonomous
	 */
	@Override
	public void autonomousPeriodic() {
		Scheduler.getInstance().run();
		updateStatus();
	}

	@Override
	public void teleopInit() {
		// This makes sure that the autonomous stops running when
		// teleop starts running. If you want the autonomous to
		// continue until interrupted by another command, remove
		// this line or comment it out.
		if (autonomousCommand != null)
			autonomousCommand.cancel();
		
		//Turn off the LEDs
		//led.setVoltage(0.0);
	}

	/**
	 * This function is called periodically during operator control
	 */
	@Override
	public void teleopPeriodic() {
		Scheduler.getInstance().run();
		updateStatus();
	}

	/**
	 * This function is called periodically during test mode
	 */
	@Override
	public void testPeriodic() {
		LiveWindow.run();
		updateStatus();
	}
	
	private void updateStatus(){
		driveUtil.updateStatus();
		gearUtil.updateStatus();
		fuelUtil.updateStatus();
		gearUtil.updateStatus();
		//visionUtil.updateStatus();
	}


}
