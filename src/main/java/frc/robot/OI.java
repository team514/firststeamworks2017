package frc.robot;

import frc.robot.commands.DriveMode;
import frc.robot.commands.DropBasket;
import frc.robot.commands.MoveLift;
import frc.robot.commands.OpenFloor;
import frc.robot.commands.OpenGear;
import frc.robot.commands.RaiseBasket;
import frc.robot.commands.ResetEncoders;
import frc.robot.commands.ResetGyro;

import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.buttons.JoystickButton;

/**
 * This class is the glue that binds the controls on the physical operator
 * interface to the commands and command groups that allow control of the robot.
 */
public class OI {
	//// CREATING BUTTONS
	// One type of button is a joystick button which is any button on a
	//// joystick.
	// You create one by telling it which joystick it's on and which button
	// number it is.
	// Joystick stick = new Joystick(port);
	// Button button = new JoystickButton(stick, buttonNumber);

	// There are a few additional built in buttons you can use. Additionally,
	// by subclassing Button you can create custom triggers and bind those to
	// commands the same as any other Button.

	//// TRIGGERING COMMANDS WITH BUTTONS
	// Once you have a button, it's trivial to bind it to a button in one of
	// three ways:

	// Start the command when the button is pressed and let it run the command
	// until it is finished as determined by it's isFinished method.
	// button.whenPressed(new ExampleCommand());

	// Run the command while the button is being held down and interrupt it once
	// the button is released.
	// button.whileHeld(new ExampleCommand());

	// Start the command when the button is released and let it run the command
	// until it is finished as determined by it's isFinished method.
	// button.whenReleased(new ExampleCommand());
	
	Joystick rightstick, leftstick, controller;
	
	JoystickButton rEncoders, rGyro, liftUp, liftUpSlow, driveMode, openGear, openFloor, dropBasket, raiseBasket, squareinputs, autoTest;
	
	public OI(){
		
		rightstick = new Joystick(0);
		leftstick = new Joystick(1);
		controller = new Joystick(2);

		rGyro = new JoystickButton(rightstick, RobotMap.ResetGyro);
		rGyro.whenPressed(new ResetGyro());

		rEncoders = new JoystickButton(rightstick, RobotMap.ResetEncoders);
		rEncoders.whenPressed(new ResetEncoders());
		
		liftUp = new JoystickButton(controller, RobotMap.kYButtonNum);
		liftUp.whileHeld(new MoveLift(false));
		
		liftUpSlow = new JoystickButton(controller, RobotMap.kXButtonNum);
		liftUpSlow.whileHeld(new MoveLift(true));
		
		openGear = new JoystickButton(controller, RobotMap.kAButtonNum);
		openGear.whenPressed(new OpenGear());
		/*
		closeGear = new JoystickButton(controller, RobotMap.kXButtonNum);
		closeGear.whenPressed(new CloseGear());
		*/
		openFloor = new JoystickButton(controller, RobotMap.kLeftBumperNum);
		openFloor.whileHeld(new OpenFloor());
		
		
		dropBasket = new JoystickButton(controller, RobotMap.kLeftTriggerNum);
		dropBasket.whenPressed(new DropBasket());
		
		raiseBasket = new JoystickButton(controller, RobotMap.kRightTriggerNum);
		raiseBasket.whenPressed(new RaiseBasket());
		
		//autoTest = new JoystickButton(controller, RobotMap.kStartButtonNum);
		//autoTest.whileHeld(new AutoGear(false));
		//autoTest.whileHeld(new AutoTurn(true));
		
		driveMode = new JoystickButton(rightstick, RobotMap.DriveMode);
		driveMode.whenPressed(new DriveMode());
	}
	
	public double getRightX(){
		return rightstick.getX();
	}
	
	public double getRightY(){
		return rightstick.getY();
	}
	
	public double getRightRotation(){
		return rightstick.getZ();
	}
	
	public double getLeftX(){
		return leftstick.getX();
	}
	
	public double getLeftY(){
		return leftstick.getY();
	}
	
	public double getLeftRotation(){
		return leftstick.getZ();
	}
	
	public double getRightDegrees(){
		return this.rightstick.getDirectionDegrees();
	}
	
	public double getRightMagnitude(){
		return this.rightstick.getMagnitude();
	}
}
