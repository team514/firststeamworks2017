package frc.robot.commands;

import frc.robot.Robot;
import frc.robot.RobotMap;

import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.command.Command;

/**
 *
 */
public class AutoGear extends Command {
	boolean side, done;
	int step, rDistance;

    public AutoGear(boolean side) {
        // Use requires() here to declare subsystem dependencies
        // eg. requires(chassis);
    	requires(Robot.driveUtil);
    	requires(Robot.gearUtil);
    	//requires(Robot.visionUtil);
    	
    	this.side = side;
    	this.step = 0;
    	this.done = false;
    	this.rDistance = 0;
    }

    // Called just before this Command runs the first time
    protected void initialize() {
		Robot.driveUtil.driveMecanum(0.0, 0.0, 0.0, false);
		Robot.driveUtil.resetGyro();
		Robot.driveUtil.resetEncoders();
    }

    // Called repeatedly when this Command is scheduled to run
    protected void execute() {
    	performAuto();
    }

    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished() {
        return this.done;
    }

    // Called once after isFinished returns true
    protected void end() {
    }

    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted() {
    }
    
    private void performAuto(){
    	switch(this.step){
    	case 0:
    		//Drive peg distance from alliance wall
    		if(Math.abs(Robot.driveUtil.getEncoders()) >= RobotMap.autoGearDistance){
    			Robot.driveUtil.driveMecanum(0.0, 0.0, 0.0, false);
    			Robot.driveUtil.resetGyro();
    			Robot.driveUtil.resetEncoders();
    			step++;
        		Timer.delay(RobotMap.wait);
    		}else{
    			Robot.driveUtil.driveAuto(RobotMap.autoY);
    		}
    		break;
    	case 1:
        	//TO.DO  -  We may want to give a PID Control a try here!!
    		if(Math.abs(Robot.driveUtil.getGyro()) >= RobotMap.turn){
    			Robot.driveUtil.driveMecanum(0.0, 0.0, 0.0, false);
    			Timer.delay(0.5);
    			Robot.driveUtil.resetGyro();
        		Robot.driveUtil.resetEncoders();
        		step++;
        		Timer.delay(0.5);
    		}else{
    			if(this.side){
    				Robot.driveUtil.driveMecanum(0.0, 0.0, RobotMap.autoZ, true);					
    			}else{
    				Robot.driveUtil.driveMecanum(0.0, 0.0, -RobotMap.autoZ, true);					
    			}
    		}
    		break;
    	case 2:
    		//TO.DO Put the Peg Switch Logic back in over the Pin Distance
    		//if(Robot.gearUtil.getpegSwitch()
    		Robot.driveUtil.driveAuto(RobotMap.autoY);
    		if(Math.abs(Robot.driveUtil.getEncoders()) >= RobotMap.autoPinDistance){
    			Robot.driveUtil.driveMecanum(0.0, 0.0, 0.0, false);
        		Robot.driveUtil.resetEncoders();
        		Robot.driveUtil.resetGyro();
        		Timer.delay(RobotMap.wait);
        		break;    			
    		}
    		if(Robot.gearUtil.getpegSwitch()){
    			Robot.driveUtil.driveMecanum(0.0, 0.0, 0.0, false);
    			Robot.gearUtil.openGear();
        		Robot.driveUtil.resetEncoders();
        		Robot.driveUtil.resetGyro();
        		Timer.delay(2.0);
        		step++;    			
    		}
    		break;
    	case 3:
    		if(Math.abs(Robot.driveUtil.getEncoders()) >= RobotMap.autoFinishDistance){
    			Robot.driveUtil.driveMecanum(0.0, 0.0, 0.0, false);
    			Robot.driveUtil.resetEncoders();
        		Robot.driveUtil.resetGyro();
        		Robot.gearUtil.openGear();
        		Timer.delay(RobotMap.wait);
        		step++; 
    		}else{
    			Robot.driveUtil.driveAuto(-RobotMap.autoY);
    			Robot.gearUtil.openGear();
    		}
    		
    		break;
    		/*
    	case 4:
        	//TO.DO  -  We may want to give a PID Control a try here!!
    		if(Math.abs(Robot.driveUtil.getGyro()) >= RobotMap.turn){
    			Robot.driveUtil.driveMecanum(0.0, 0.0, 0.0, false);
    			Robot.driveUtil.resetGyro();
        		Robot.driveUtil.resetEncoders();
        		step++;
        		Timer.delay(RobotMap.wait);
    		}else{
    			if(this.side){
    				Robot.driveUtil.driveMecanum(0.0, 0.0, -RobotMap.autoZ, true);					
    			}else{
    				Robot.driveUtil.driveMecanum(0.0, 0.0, RobotMap.autoZ, true);					
    			}
    		}
    		break;
    	case 5:
    		if(Math.abs(Robot.driveUtil.getEncoders()) >= RobotMap.autoFinish){
    			Robot.driveUtil.driveMecanum(0.0, 0.0, 0.0, false);
    			Robot.driveUtil.resetGyro();
    			Robot.driveUtil.resetEncoders();
    			step++;
    			Timer.delay(RobotMap.wait);
    			this.done = true;
    		}else{
    			Robot.driveUtil.driveAuto(RobotMap.autoY);
    		}
    		break;*/
    	default:
    		Robot.driveUtil.driveMecanum(0.0, 0.0, 0.0, false);
    		break;
    	}
    }
}
