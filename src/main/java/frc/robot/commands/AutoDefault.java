package frc.robot.commands;

import frc.robot.Robot;
import frc.robot.RobotMap;

import edu.wpi.first.wpilibj.command.Command;

/**
 *
 */
public class AutoDefault extends Command {
	boolean done = false;

    public AutoDefault() {
        // Use requires() here to declare subsystem dependencies
        // eg. requires(chassis);
    	requires(Robot.driveUtil);
    }

    // Called just before this Command runs the first time
    protected void initialize() {
    }

    // Called repeatedly when this Command is scheduled to run
    protected void execute() {
        	Robot.driveUtil.driveAuto(RobotMap.autoY);
        	if(Math.abs(Robot.driveUtil.getEncoders()) >= RobotMap.autoGearDistance){
    			Robot.driveUtil.driveMecanum(0.0, 0.0, 0.0, false);
    			Robot.driveUtil.resetGyro();
    			Robot.driveUtil.resetEncoders();
    			done = true;
        	}
    }

    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished() {
        return done;
    }

    // Called once after isFinished returns true
    protected void end() {
    }

    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted() {
    }
}
