package frc.robot.commands;

import frc.robot.Robot;
import frc.robot.RobotMap;

import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.command.Command;

/**
 *
 */
public class AutoCenterGear extends Command {
	boolean deliveredPeg;
    public AutoCenterGear() {
        // Use requires() here to declare subsystem dependencies
        // eg. requires(chassis);
    	requires(Robot.gearUtil);
    	requires(Robot.driveUtil);
    	deliveredPeg = false;
    }

    // Called just before this Command runs the first time
    protected void initialize() {
		Robot.driveUtil.driveMecanum(0.0, 0.0, 0.0, false);
		Robot.driveUtil.resetGyro();
		Robot.driveUtil.resetEncoders();
    }

    // Called repeatedly when this Command is scheduled to run
    protected void execute() {
    	if(!deliveredPeg){
    		Robot.driveUtil.driveAuto(RobotMap.autoY);
    		if(Robot.gearUtil.getpegSwitch()){
    			Robot.driveUtil.driveMecanum(0.0, 0.0, 0.0, false);
    			Timer.delay(1.0);
    			Robot.gearUtil.openGear();
    			Timer.delay(1.0);
    			Robot.driveUtil.resetEncoders();
    			deliveredPeg = true;
    		}
    		return;
    	}
    	if(Math.abs(Robot.driveUtil.getEncoders()) <= RobotMap.autoFinishDistance){
    		Robot.gearUtil.openGear();
    		//Timer.delay(1.0);
    		Robot.driveUtil.driveAuto(-RobotMap.autoY);
    	}else{
    		Robot.gearUtil.openGear();
    		Robot.driveUtil.driveMecanum(0.0, 0.0, 0.0, false);
    		Timer.delay(1.0);
    	}
    }

    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished() {
    	boolean done = false;
    	if(deliveredPeg && (Math.abs(Robot.driveUtil.getEncoders()) >= RobotMap.autoFinishDistance)){
    		Robot.driveUtil.driveMecanum(0.0, 0.0, 0.0, false);
    		done = true;
    	}
        return done;
    }

    // Called once after isFinished returns true
    protected void end() {
    }

    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted() {
    }
}
