package frc.robot.commands;

import frc.robot.Robot;
import frc.robot.RobotMap;

import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.command.Command;

/**
 *
 */
public class AutoTurn extends Command {
	boolean side;
    public AutoTurn(boolean side) {
        // Use requires() here to declare subsystem dependencies
        // eg. requires(chassis);
    	requires(Robot.driveUtil);
    	this.side = side;
    }

    // Called just before this Command runs the first time
    protected void initialize() {
    }

    // Called repeatedly when this Command is scheduled to run
    protected void execute() {
    	//TO.DO  -  We may want to give a PID Control a try here!!
		if(Math.abs(Robot.driveUtil.getGyro()) >= RobotMap.turn){
			Robot.driveUtil.driveMecanum(0.0, 0.0, 0.0, false);
			Robot.driveUtil.resetGyro();
    		Robot.driveUtil.resetEncoders();
    		Timer.delay(0.5);
		}else{
			if(this.side){
				Robot.driveUtil.driveMecanum(0.0, 0.0, RobotMap.autoZ, true);					
			}else{
				Robot.driveUtil.driveMecanum(0.0, 0.0, -RobotMap.autoZ, true);					
			}
		}
    }

    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished() {
        return false;
    }

    // Called once after isFinished returns true
    protected void end() {
    }

    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted() {
    }
}
