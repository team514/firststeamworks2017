package frc.robot.subsystems;

import frc.robot.RobotMap;

import edu.wpi.cscore.UsbCamera;
import edu.wpi.first.wpilibj.CameraServer;
//import edu.wpi.first.wpilibj.Sendable;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj.vision.VisionRunner;
import edu.wpi.first.wpilibj.vision.VisionThread;

/**
 *
 */
@SuppressWarnings("deprecation")
public class VisionUtil extends Subsystem  implements VisionRunner.Listener<VisionProcessingPipeline> {

    // Put methods for controlling this subsystem
    // here. Call these from Commands.
    // A USB camera connected to the roboRIO.
	
    private UsbCamera usbCam;

    //The pipeline outputs we want are in this class object
    public VisionData visionData;

    //TO.DO:  Should be able to stop the running thread processing images after autonomous ends... 
	private VisionProcessingPipeline ourPipeline;
	private VisionThread visionThread;

    // The object to synchronize on to make sure the vision thread doesn't
    // write to variables the main thread is using.
    private final Object visionLock = new Object();

	public VisionUtil(){
		//Empty constructor
	}
	
	public void startVisionProcessing(){
		usbCam = CameraServer.getInstance().startAutomaticCapture();
		usbCam.setResolution(RobotMap.xresUsb, RobotMap.yresUsb);
		ourPipeline = new VisionProcessingPipeline();
		visionThread = new VisionThread(usbCam, ourPipeline, this);
		visionThread.start();
	}
	
	
    public void initDefaultCommand() {
        // Set the default command for a subsystem here.
        //setDefaultCommand(new MySpecialCommand());
    }
    
    public void updateStatus(){
    	if(visionData != null){
        	synchronized (visionLock){
            	if(Boolean.toString(visionData.onTarget) != null){
            		SmartDashboard.putBoolean("Robot is on Target : ", visionData.onTarget);            		
            	}
            	if(Double.toString(visionData.TargetCMX) != null){
            		SmartDashboard.putNumber("Target CMX : ", visionData.TargetCMX);        		
            	}
            	if(Double.toString(visionData.distanceToGearPeg) != null){
                	SmartDashboard.putNumber("Distance to Target : ", visionData.distanceToGearPeg);        		
            	}
            	if(visionData.xDirection != null){
                	SmartDashboard.putString("Target Orientation : ", visionData.xDirection);        		
            	}
            	if(Boolean.toString(visionData.hasTarget) != null){
                	SmartDashboard.putBoolean("Pipeline Sees Targets : ", visionData.hasTarget);    		        		
            	}
            	if(Double.toString(visionData.scoreAR) != null){
            		SmartDashboard.putNumber("Score Aspect Ratio : " , visionData.scoreAR);
            	}
            	if(Double.toString(visionData.scoreRect) != null){
            		SmartDashboard.putNumber("Score Rectangularity : ", visionData.scoreRect);
            	}
            	if(Double.toString(visionData.targetHeight) != null){
            		SmartDashboard.putNumber("Target Height: ", visionData.targetHeight);
            	}
            	if(Double.toString(visionData.targetWidth) != null){
            		SmartDashboard.putNumber("Target Width: ", visionData.targetWidth);
            	}
            	if(Double.toString(visionData.brx) != null){
            		SmartDashboard.putNumber("Bounding Rectangle X-Value: ", visionData.brx);
            	}
            	if(Double.toString(visionData.bry) != null){
            		SmartDashboard.putNumber("Bounding Rectangle Y-Value: ", visionData.bry);
            	}
            	
        	}    		
    	}
    }
    
	@Override
	public void copyPipelineOutputs(VisionProcessingPipeline pipeline) {
        synchronized (visionLock) {
        	this.visionData = new VisionData(pipeline.getDistanceToTarget(),
        									 pipeline.onTarget(),
        									 pipeline.getXDirection(),
        									 pipeline.getTargetCMX(),
        									 pipeline.getHasTarget(),
        									 pipeline.getScoreAR(),
        									 pipeline.getScoreRect(),
        									 pipeline.getTargetHeight(),
        									 pipeline.getTargetWidth(),
        									 pipeline.getBRX(),
        									 pipeline.getBRY());
            }
    }
	
	//This method is called by commands that need Vision Processing data to work.
	public VisionData getVisionData(){
		synchronized (visionLock){
			return this.visionData;			
		}
	}
}

