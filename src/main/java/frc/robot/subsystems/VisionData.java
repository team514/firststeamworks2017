package frc.robot.subsystems;

public final class VisionData {
	/**
	 * This is a single class that encapsulates the data that we 
	 * need to get from the Vision Pipeline.
	 * We will create a synchronized lock on this class
	 * All reads and writes of Vision Pipeline data will
	 * Sync on this object.
	 */
	
    public final double distanceToGearPeg;
    public final boolean onTarget;
    public final String xDirection;
    public final double TargetCMX;
    public final boolean hasTarget;
    public final double scoreAR;
    public final double scoreRect;
    public final double targetHeight;
    public final double targetWidth;
    public final double brx;
    public final double bry;

	public VisionData(double calcDistanceToGearPeg, 
					  boolean onTarget2, 
					  String xDirection2,
					  double targetCMX2, 
					  boolean hasTarget2,
					  double scoreAR2,
					  double scoreRect2,
					  double targetHeight2,
					  double targetWidth2,
					  double brx2,
					  double bry2
					  ){
 
			distanceToGearPeg = calcDistanceToGearPeg;
			onTarget = onTarget2;
			xDirection = xDirection2;
			TargetCMX = targetCMX2;
			hasTarget = hasTarget2;
			scoreAR = scoreAR2;
			scoreRect = scoreRect2;
			targetHeight = targetHeight2;
			targetWidth = targetWidth2;
			brx = brx2;
			bry = bry2;
	}

}
