package frc.robot.subsystems;

import frc.robot.RobotMap;
import frc.robot.commands.DriveBot;

import edu.wpi.first.wpilibj.ADXRS450_Gyro;
import edu.wpi.first.wpilibj.Encoder;
import edu.wpi.first.wpilibj.SPI.Port;
import edu.wpi.first.wpilibj.SpeedController;
import edu.wpi.first.wpilibj.Victor;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.drive.MecanumDrive;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

/**
 *
 */
public class DriveUtil extends Subsystem {

    // Put methods for controlling this subsystem
    // here. Call these from Commands.
	SpeedController leftfront, rightfront, leftrear, rightrear;
	MecanumDrive drive;
	ADXRS450_Gyro gyro;
	Encoder encoderlr, encoderlf, encoderrf, encoderrr;
	double speed, adjustment;
	boolean arcade;
	
	public DriveUtil(){
		leftrear = new Victor(RobotMap.leftrear);
		rightrear = new Victor(RobotMap.rightrear);
		leftfront = new Victor(RobotMap.leftfront);
		rightfront = new Victor(RobotMap.rightfront);
		drive = new MecanumDrive(leftfront, leftrear, rightfront, rightrear);
//		drive.setInvertedMotor(MotorType.kRearLeft, true);
		//drive.setInvertedMotor(MotorType.kFrontLeft, true);
		drive.setSafetyEnabled(true);
//		leftrear.setInverted(true);
//		leftfront.setInverted(true);
		gyro = new ADXRS450_Gyro(Port.kOnboardCS0);
		encoderlr = new Encoder(RobotMap.leftrearforward, RobotMap.leftrearbackward);
		encoderlf = new Encoder(RobotMap.leftfrontforward, RobotMap.leftfrontbackward);
		encoderrf = new Encoder(RobotMap.rightfrontforward, RobotMap.rightfrontbackward);
		encoderrr = new Encoder(RobotMap.rightrearforward, RobotMap.rightrearbackward);
		this.speed = 0.0;
		this.arcade = true;
		resetEncoders();
		resetGyro();
	}
	
	public boolean isArcade(){
		return this.arcade;
	}
	
	public void resetGyro(){
		gyro.reset();
	}
	
	public void calibrateGyro(){
		gyro.calibrate();
	}
	
	public double getGyro(){
		return gyro.getAngle();
	}
	
	public void driveMecanum(double x, double y, double z, boolean useGyro){
		x = squareInputs(x);
		y = squareInputs(y);
		z = squareInputs(z);
		if(useGyro){
			drive.driveCartesian(-x, y, -z, gyro.getAngle());
		}else{
			drive.driveCartesian(-x, y, -z, 0);
		}
	}
	
	public void driveAuto(double yAxis){
		double adj = coerce2Range(getGyro());
		//left = left + adj;
		//right = right + -adj;
		this.adjustment = -adj;
		driveMecanum(0.0, yAxis, -adj, true);
	}
	
	public void driveMagic(double yAxis, int pixelOffset){
		double adj = coerce2RangeMagic(pixelOffset);
		this.adjustment = adj;
		driveMecanum(adj, yAxis, 0.0, true);
	}
	
	public double coerce2RangeMagic(int input){
        // TO.DO code application logic here
        double inputMin, inputMax, inputCenter;
        double outputMin, outputMax, outputCenter;
        double scale, result;
        //double output;
        
        inputMin = RobotMap.C2RM_inputMin; 
        inputMax = RobotMap.C2RM_inputMax;     
        
        outputMin = RobotMap.C2RM_outputMin;
        outputMax = RobotMap.C2RM_outputMax;
        
        /* Determine the center of the input range and output range */
            inputCenter = Math.abs(inputMax - inputMin) / 2 + inputMin;
            outputCenter = Math.abs(outputMax - outputMin) / 2 + outputMin;

            /* Scale the input range to the output range */
            scale = (outputMax - outputMin) / (inputMax - inputMin);

            /* Apply the transformation */
            result = (input + -inputCenter) * scale + outputCenter;

            /* Constrain to the output range */
            speed = Math.max(Math.min(result, outputMax), outputMin);

       return speed;
	}
	
	private double squareInputs(double sq){
		if(sq < 0.0){
			sq = ((sq * sq)*-1);
		}else{
			sq = (sq * sq);
		}
		return sq;	
	}
	
	public void resetEncoders(){
		this.encoderlr.reset();
		this.encoderlf.reset();
		this.encoderrr.reset();
		this.encoderrf.reset();
	}
	
	public int getEncoders(){
		return encoderrr.get();
	}
	
    public double coerce2Range(double input){
        // TO.DO code application logic here
        double inputMin, inputMax, inputCenter;
        double outputMin, outputMax, outputCenter;
        double scale, result;
        //double output;
        
        inputMin = RobotMap.C2R_inputMin; 
        inputMax = RobotMap.C2R_inputMax;     
        
        outputMin = RobotMap.C2R_outputMin;
        outputMax = RobotMap.C2R_outputMax;
        
        /* Determine the center of the input range and output range */
            inputCenter = Math.abs(inputMax - inputMin) / 2 + inputMin;
            outputCenter = Math.abs(outputMax - outputMin) / 2 + outputMin;

            /* Scale the input range to the output range */
            scale = (outputMax - outputMin) / (inputMax - inputMin);

            /* Apply the transformation */
            result = (input + -inputCenter) * scale + outputCenter;

            /* Constrain to the output range */
            speed = Math.max(Math.min(result, outputMax), outputMin);

       return speed;
       
    }
	
    public void initDefaultCommand() {
        // Set the default command for a subsystem here.
        //setDefaultCommand(new MySpecialCommand());
    	setDefaultCommand(new DriveBot());
    }
    
    public void updateStatus(){
    	SmartDashboard.putNumber("Gyro Angle : ", gyro.getAngle());
    	SmartDashboard.putNumber("Left Rear Encoder : ", encoderlr.getDistance());
    	SmartDashboard.putNumber("Left Front Encoder : ", encoderlf.getDistance());
    	SmartDashboard.putNumber("Right Rear Encoder : ", encoderrr.getDistance());
    	SmartDashboard.putNumber("Right Front Encoder : ", encoderrf.getDistance());
    	SmartDashboard.putNumber("Speed Adjustment Z : ", this.adjustment);
    }

	public void toggleArcade() {
		this.arcade = !this.arcade;
	}
}

