package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;

import edu.wpi.first.wpilibj.command.Subsystem;
import frc.robot.RobotMap;
import frc.robot.commands.StopLift;

/**
 *
 */
public class LiftUtil extends Subsystem {

    // Put methods for controlling this subsystem
    // here. Call these from Commands.
	WPI_TalonSRX liftmotor;
	
	public LiftUtil(){
		liftmotor = new WPI_TalonSRX(RobotMap.liftmotor);
		//liftmotor = new CANTalon(RobotMap.liftmotor);
	}
	
	public void operateLift(double d){
		liftmotor.set(ControlMode.PercentOutput, d);
		
//		liftmotor.set(d);
		System.out.println(d);
	}

    public void initDefaultCommand() {
        // Set the default command for a subsystem here.
        //setDefaultCommand(new MySpecialCommand());
    	setDefaultCommand(new StopLift());
    }
}
