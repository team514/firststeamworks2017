package frc.robot.subsystems;

import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.Solenoid;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.robot.RobotMap;

/**
 *
 */
public class GearUtil extends Subsystem {
	
    // Put methods for controlling this subsystem
    // here. Call these from Commands.

	Solenoid gearGrip;
	DigitalInput pegSwitch;
	
	public GearUtil(){
		gearGrip = new Solenoid(RobotMap.gearGrip);
		pegSwitch = new DigitalInput(RobotMap.pegSwitch);
	
	}
	public void openGear(){
		gearGrip.set(!gearGrip.get());
	}
	
	public void closeGear(){
		gearGrip.set(false);
	}
	public boolean getpegSwitch(){
		return !pegSwitch.get();
	}
	
	
    public void initDefaultCommand() {
        // Set the default command for a subsystem here.
        //setDefaultCommand(new MySpecialCommand());
    	// setDefaultCommand(new CloseGear());
    }
    
    public void updateStatus(){
    	SmartDashboard.putBoolean("Gear Open/Close: ", gearGrip.get());
		SmartDashboard.putBoolean("Peg Switch Touching: ", getpegSwitch());
    }
}

